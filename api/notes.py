class Note(object):

	amount = 12

	def __init__(self, note: str, sharp=False):
		self.note =  note
		self.sharp = sharp
		self.title = note + '#' if self.sharp else note

	def info(self):
		return {
			'note': self.note,
			'sharp': self.sharp,
			'title': self.title
		}


def get_note_index(note_info):

	for idx, note in enumerate(notes):

		if note.info() == note_info:
			return idx

	return -1

notes = [
	Note('A'), Note('A', sharp=True), Note('B'),
	Note('C'), Note('C', sharp=True), Note('D'),
	Note('D', sharp=True), Note('E'), Note('F'),
	Note('F', sharp=True), Note('G'), Note('G', sharp=True)
]
