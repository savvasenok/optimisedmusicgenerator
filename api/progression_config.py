from api.text import *

progressions = (
	(0, 2, 4),
	(0, 2, 3, 4),
	(0, 2, 3, 4, 5),
	(0, 3, 4),
	(0, 3, 4, 5, 1, 4, 0),
	(0, 3, 5),
	(0, 3, 6),
	(0, 4, 5, 3),
	(0, 5, 1, 4),
	(0, 5, 2, 3, 4),
	(0, 5, 3, 4),
	(0, 6, 1, 4),
	(0, 6, 3, 4),
)

major_degrees = {
	0: major,
	1: minor,
	2: minor,
	3: major,
	4: major,
	5: minor,
	6: dim
}

minor_degrees = {
	0: minor,
	1: dim,
	2: major,
	3: minor,
	4: major,
	5: major,
	6: dim
}