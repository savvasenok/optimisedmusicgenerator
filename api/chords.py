from api import notes as n


class ChordBuilder(object):

	def __init__(self):
		# Add this values to jump 
		# to the next note in this scale
		self.major_scale = (2, 2, 1, 2, 2, 2)
		self.minor_scale = (2, 1, 2, 2, 1, 2)

		# Scale degrees
		self.tonic_triad = (0, 2, 4)

		# half-steps to the next note
		self.dim = (0, 3, 6)
		self.sus4 = (0, 5, 7)

	def __build_chord_of_notes__(self, notes_ids):
		return [n.notes[i].info() for i in notes_ids]

	def __build_chord_with_triads__(self, scale_ids):
		return [
			scale_ids[self.tonic_triad[0]],
			scale_ids[self.tonic_triad[1]],
			scale_ids[self.tonic_triad[2]]
		]

	def __build_note_title__(self, note_id):

		note = n.notes[note_id]
		result = note.note

		if note.sharp:
			result += '#'

		return result


	def build_gamma(self, note_id, major: bool):
		scale = self.major_scale if major else self.minor_scale

		gamma = [note_id]
		for i, idx in enumerate(scale):
			gamma.append((gamma[i] + idx) % n.Note.amount)

		return gamma

	def major(self, note_id):

		gamma = self.build_gamma(note_id, True)
		notes_ids = self.__build_chord_with_triads__(gamma)
		chord_notes = self.__build_chord_of_notes__(notes_ids)

		return {
			'title': self.__build_note_title__(note_id),
			'notes': chord_notes
		}

	def minor(self, note_id):

		gamma = self.build_gamma(note_id, False)
		notes_ids = self.__build_chord_with_triads__(gamma)
		chord_notes = self.__build_chord_of_notes__(notes_ids)

		return {
			'title': self.__build_note_title__(note_id) + 'm',
			'notes': chord_notes
		}

	def diminished(self, note_id):

		notes_ids = [
			(note_id + self.dim[0]) % n.Note.amount,
			(note_id + self.dim[1]) % n.Note.amount,
			(note_id + self.dim[2]) % n.Note.amount
		]

		chord_notes = self.__build_chord_of_notes__(notes_ids)

		return {
			'title': self.__build_note_title__(note_id) + 'dim',
			'notes': chord_notes
		}

	def sept(self, chord):

		# "chord" here - is returned dict
		# of some chord
		notes_ids = [n.get_note_index(info) for info in chord['notes']]
		notes_ids.append((notes_ids[0] + 10) % n.Note.amount)

		chord_notes = self.__build_chord_of_notes__(notes_ids)

		return {
			'title': chord['title'] + '7',
			'notes': chord_notes
		}

	def suspended4(self, note_id):

		gamma = self.build_gamma(note_id, True)
		notes_ids = [
			(note_id + self.sus4[0]) % n.Note.amount,
			(note_id + self.sus4[1]) % n.Note.amount,
			(note_id + self.sus4[2]) % n.Note.amount,
		]
		chord_notes = self.__build_chord_of_notes__(notes_ids)

		return {
			'title': self.__build_note_title__(note_id) + 'sus4',
			'notes': chord_notes
		}


	def get_all_chords(self, note_id):
		all_chords = [
			self.major(note_id),
			self.minor(note_id),
			self.diminished(note_id)
		]

		return all_chords


chord_builder = ChordBuilder()