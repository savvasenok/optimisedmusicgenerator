from api import notes as n
from api.chords import * 
from api.text import *
from api.progression_config import *
from re import search


def make_progression(chords):

	if len(chords) == 1:
		return process_one_chord(chords[0])

	return process_multiple_chords(chords)


def process_multiple_chords(chords):

	pos_progressions = {}

	for i, root in enumerate(chords):

		mood = is_major(root)
		note = find_root(root)
		note_id = n.get_note_index(note)
		gamma = chord_builder.build_gamma(note_id, mood)

		pos_subprogressions = []

		# enumerate through all chords, except one,
		# that we think is root one
		for j, subchord in enumerate(chords[:i] + chords[i+1:]):

			submood = is_major(subchord)
			subnote = find_root(subchord)
			subnote_id = n.get_note_index(subnote)

			if subnote_id in gamma:
				#find possible progressions
				subchord_degree = gamma.index(subnote_id)
				pos_subprogressions.append(subchord_degree)

		similar_progressions = get_similar_progressions(pos_subprogressions)
		if similar_progressions:

			similar_progressions_intervals = []
			for prog in similar_progressions:
				for degree in prog:

					if degree not in similar_progressions_intervals:
						similar_progressions_intervals.append(degree)


			pos_progressions[root] = similar_progressions_intervals

	return chords_from_root_by_progression(pos_progressions)


def get_similar_progressions(pos_progression):

	# returns all progressions, that are similar
	result = []

	for prog in progressions:

		union = sorted(set(pos_progression) & set(prog))
		if union == sorted(pos_progression) and union:

			result.append(prog)

	return result

def chords_from_root_by_progression(progressions):

	all_chords = []

	for key, degrees in progressions.items():

		note = find_root(key)
		mood = is_major(key)
		note_id = n.get_note_index(note)
		scale = minor_degrees \
				if not mood \
				else major_degrees

		gamma = chord_builder.build_gamma(note_id, mood)

		for i in degrees:

			new_chord = None

			if scale[i] == major:
				new_chord = chord_builder.major(gamma[i])
			if scale[i] == minor:
				new_chord = chord_builder.minor(gamma[i])
			if scale[i] == dim:
				new_chord = chord_builder.diminished(gamma[i])

			info = {'title': new_chord['title'], 'notes': new_chord['notes']}

			if info not in all_chords:
				all_chords.append(info)

	return all_chords

def process_one_chord(chord):
	all_chords = []

	for prog in progressions:

		note = find_root(chord)
		mood = is_major(chord)
		note_id = n.get_note_index(note)
		scale = minor_degrees \
				if not mood \
				else major_degrees

		gamma = chord_builder.build_gamma(note_id, mood)
		note_ids = [gamma[i] for i in prog]


		for degree, note_id in enumerate(note_ids):

			new_chord = None

			if scale[prog[degree]] == major:
				new_chord = chord_builder.major(note_id)

			elif scale[prog[degree]] == minor:
				new_chord = chord_builder.minor(note_id)

			elif scale[prog[degree]] == dim:
				new_chord = chord_builder.diminished(note_id)

			info = {'title': new_chord['title'], 'notes': new_chord['notes']}

			if info not in all_chords:
				all_chords.append(info)

	return all_chords

def is_major(chord):
	return not bool(search('[A-Z#|A-Z](m)', chord))

def find_root(chord):
	note = chord.replace(sept, '')
	note = note.replace(sus4, '')
	note = note.replace(dim,  '')
	note = note.replace('m',  '')

	info = {
		'note':  note.replace('#', ''),
		'sharp': '#' in note,
		'title': note
	}

	return info
