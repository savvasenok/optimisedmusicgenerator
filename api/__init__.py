from api import text
from api import notes as n
from api.chords import chord_builder
from api.progressions import *
from flask import Flask, request
from flask_cors import CORS, cross_origin
from config import Config
import json


app = Flask(__name__)
cors = CORS(app)
app.config.from_object(Config)


@app.route('/', methods=['GET'])
@cross_origin()
def index():

	return text.welcome

@app.route('/note', methods=['GET'])
@cross_origin()
def start_notes():

	return json.dumps(list(note.title for note in n.notes))

@app.route('/start_chord', methods=['GET'])
@cross_origin()
def start_chord():

	note_info = {
		'note':  request.args.get('note').replace('#', ''),
		'sharp': '#' in request.args.get('note'),
		'title': request.args.get('note')
	}

	note_index = n.get_note_index(note_info)

	if note_index != -1:
		return json.dumps(
			chord_builder.get_all_chords(note_index)
		)

	return text.error

@app.route('/chords', methods=['GET'])
@cross_origin()
def calculate_chords():

	chords = request.args.getlist('chords')

	return json.dumps(make_progression(chords))